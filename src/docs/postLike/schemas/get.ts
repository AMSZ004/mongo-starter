/**
 * @swagger
 * components:
 *   securitySchemes:
 *      bearerAuth:
 *          type: http
 *          scheme: bearer
 *          bearerFormat: JWT
 */
/**
 * @swagger
 * components:
 *   schemas:
 *     getPostLike:
 *       type: object
 *       properties:
 *         user:
 *           type: string
 *           description: the user like the post 
 *         post:
 *           type: string
 *           description: the post reference 
 */
