/**
 * @swagger
 * /{postId}/likes/{likeId}:
 *   get:
 *     summary: Get One like
 *     tags: [Likes]
 *     parameters:
 *       - in: path
 *         name: postId
 *         schema:
 *           type: string
 *         required: true
 *         description: The post id
 *       - in: path
 *         name: likeId
 *         schema:
 *           type: string
 *         required: true
 *         description: The like id
 *     responses:
 *       200:
 *         description: Like returned successfully
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/getPostLike'
 *       404:
 *         description: Post not found
 *     security:
 *       - bearerAuth: []
 */