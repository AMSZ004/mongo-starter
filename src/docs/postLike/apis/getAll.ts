/**
 * @swagger
 * /{postId}/likes:
 *   get:
 *    summary: Returns the list of all the Posts
 *    tags: [Likes]
 *    parameters:
 *        - in: path
 *          name: postId
 *          schema:
 *            type: string
 *          required: true
 *          description: The post id
 *        - in: query
 *          name: status
 *          schema:
 *            type: string
 *        - in: query
 *          name: sort
 *          schema:
 *            type: string
 *        - in: query
 *          name: deleted
 *          schema:
 *            type: string
 *        - in: query
 *          name: page
 *          schema:
 *            type: integer
 *        - in: query
 *          name: perPage
 *          schema:
 *            type: integer
 *    responses:
 *       200:
 *         description: All likes returned successfully
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/getPostLike'
 *    security:
 *      - bearerAuth: []
 */
