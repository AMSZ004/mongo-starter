/**
 * @swagger
 * /{postId}/likes/{likeId}:
 *   patch:
 *    summary: update like
 *    tags: [Likes]
 *    parameters:
 *       - in: path
 *         name: postId
 *         schema:
 *           type: string
 *         required: true
 *         description: The post id
 *       - in: path
 *         name: likeId
 *         schema:
 *           type: string
 *         required: true
 *         description: The like id
 *    requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/getPostLike'
 *    responses:
 *       200:
 *         description: Like updated successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/getPostLike'
 *       404:
 *         description: Post not found
 *       500:
 *         description: Some server error
 *    security:
 *      - bearerAuth: []
 */
