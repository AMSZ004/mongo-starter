
/**
 * @swagger
 * /{postId}/likes:
 *   post:
 *     summary: Create a new like  
 *     tags: [Likes]
 *     parameters:
 *         - in: path
 *           name: postId
 *           schema:
 *             type: string
 *           required: true
 *           description: The post id
 *     responses:
 *       200:
 *         description: Like has been created successfully!
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/createPostLike'
 *       500:
 *         description: Some server error
 *     security:
 *        - bearerAuth: []
 */
