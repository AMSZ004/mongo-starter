/**
 * @swagger
 * components:
 *   securitySchemes:
 *      bearerAuth:
 *          type: http
 *          scheme: bearer
 *          bearerFormat: JWT
 */
/**
 * @swagger
 * components:
 *   schemas:
 *     createPostMedia:
 *       type: object
 *       properties:
 *         description:
 *           type: string
 *           description: description of the post (e.g., text description)
 *         mediaUrl:
 *           type: string
 *           format: binary
 *           description: mediaUrl video or image
 *     createPostCard:
 *       type: object
 *       properties:
 *         scoreCard:
 *           type: object
 *           properties:
 *             teamScore:
 *               type: integer
 *               description: My Team Score
 *             opponentScore:
 *               type: integer
 *               description: Opponent Score
 *             matchDate:
 *               type: string
 *               format: date
 *               description: Match Date
 *             goals:
 *               type: integer
 *               description: goals
 *             assists:
 *               type: integer
 *               description: assists
 *             keyPasses:
 *               type: integer
 *               description: keyPasses
 *             nutmegs:
 *               type: integer
 *               description: nutmegs
 */
