/**
 * @swagger
 * /posts/{postId}:
 *   patch:
 *    summary: update post
 *    tags: [Posts]
 *    parameters:
 *       - in: path
 *         name: postId
 *         schema:
 *           type: string
 *         required: true
 *         description: The post id
 *    requestBody:
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             $ref: '#/components/schemas/updatePost'
 *    responses:
 *       200:
 *         description: Post updated successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/getPosts'
 *       404:
 *         description: Post not found
 *       500:
 *         description: Some server error
 *    security:
 *      - bearerAuth: []
 */
/**
 * @swagger
 * /posts/{postId}/card:
 *   patch:
 *    summary: update post card
 *    tags: [Posts]
 *    parameters:
 *       - in: path
 *         name: postId
 *         schema:
 *           type: string
 *         required: true
 *         description: The post id
 *    requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/updatePostCard'
 *    responses:
 *       200:
 *         description: Post updated successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/getPosts'
 *       404:
 *         description: Post not found
 *       500:
 *         description: Some server error
 *    security:
 *      - bearerAuth: []
 */

/**
 * @swagger
 * /posts/{postId}/status:
 *   patch:
 *    summary: update post status
 *    tags: [Posts]
 *    parameters:
 *       - in: path
 *         name: postId
 *         schema:
 *           type: string
 *         required: true
 *         description: The post id
 *    requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/updatePostStatus'
 *    responses:
 *       200:
 *         description: Post updated successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/getPosts'
 *       404:
 *         description: Post not found
 *       500:
 *         description: Some server error
 *    security:
 *      - bearerAuth: []
 */
