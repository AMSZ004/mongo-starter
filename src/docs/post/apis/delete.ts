/**
 * @swagger
 * /posts/{postId}:
 *   delete:
 *     summary: Delete One post
 *     tags: [Posts]
 *     parameters:
 *       - in: path
 *         name: postId
 *         schema:
 *           type: string
 *         required: true
 *         description: The post id
 *     responses:
 *       200:
 *         description: Post deleted successfully
 *       404:
 *         description: Post not found
 *     security:
 *       - bearerAuth: []
 */