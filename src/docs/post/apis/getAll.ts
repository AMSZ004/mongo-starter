/**
 * @swagger
 * /posts:
 *   get:
 *    summary: Returns the list of all the Posts
 *    tags: [Posts]
 *    parameters:
 *        - in: query
 *          name: status
 *          schema:
 *            type: string
 *        - in: query
 *          name: sort
 *          schema:
 *            type: string
 *        - in: query
 *          name: deleted
 *          schema:
 *            type: string
 *        - in: query
 *          name: page
 *          schema:
 *            type: integer
 *        - in: query
 *          name: perPage
 *          schema:
 *            type: integer
 *    responses:
 *       200:
 *         description: All posts returned successfully
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/getPosts'
 *    security:
 *      - bearerAuth: []
 */
