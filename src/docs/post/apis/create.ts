/**
 * @swagger
 * /posts/media:
 *   post: 
 *     summary: Create a new post media
 *     tags: [Posts]
 *     requestBody:
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             $ref: '#/components/schemas/createPostMedia'
 *     responses:
 *       200:
 *         description: Post has been created successfully!
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/getPosts'
 *       500:
 *         description: Some server error
 *     security:
 *       - bearerAuth: []
 */
/**
 * @swagger
 * /posts/short:
 *   post:
 *     summary: Create a new post short
 *     tags: [Posts]
 *     requestBody:
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             $ref: '#/components/schemas/createPostMedia'
 *     responses:
 *       200:
 *         description: Post has been created successfully!
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/getPosts'
 *       500:
 *         description: Some server error
 *     security:
 *       - bearerAuth: []
 */
/**
 * @swagger
 * /posts/card:
 *   post:
 *     summary: Create a new post card
 *     tags: [Posts]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/createPostCard'
 *     responses:
 *       200:
 *         description: Post has been created successfully!
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/getPosts'
 *       500:
 *         description: Some server error
 *     security:
 *        - bearerAuth: []
 */
