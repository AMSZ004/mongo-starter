/**
 * @swagger
 * /posts/{postId}:
 *   get:
 *     summary: Get One post
 *     tags: [Posts]
 *     parameters:
 *       - in: path
 *         name: postId
 *         schema:
 *           type: string
 *         required: true
 *         description: The post id
 *     responses:
 *       200:
 *         description: Post returned successfully
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/getPosts'
 *       404:
 *         description: Post not found
 *     security:
 *       - bearerAuth: []
 */

