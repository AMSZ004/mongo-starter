import Joi from "@hapi/joi";
import { JoiObjectId } from "../../../helpers/validator";



export default {
    likeId: Joi.object().keys({
        postId: JoiObjectId().required(),
        userId: JoiObjectId().optional()
    })
   
}