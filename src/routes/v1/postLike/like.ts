import express from "express";
import authentication from "../../../auth/authentication";
import authorization from "../../../auth/authorization";
import { RoleCode } from "../../../database/model/Role";
import postLike from "../../../controllers/postLike";
import validator from "../../../helpers/validator";
import { ValidationSource } from "../../../helpers/validator";
import Schema from "./Schema";

const router = express.Router({
  mergeParams: true,
});

router.use("/", authentication, authorization([RoleCode.ADMIN, RoleCode.USER]));
router.post(
  "/",
  validator(Schema.likeId, ValidationSource.PARAM),
  postLike.createLike
);

router.get(
  "/",
  validator(Schema.likeId, ValidationSource.PARAM),
  postLike.getAllLike
);

router.get(
  "/:likeId",
  validator(Schema.likeId, ValidationSource.PARAM),
  postLike.getLike
);

router.patch(
  "/:likeId",
  validator(Schema.likeId, ValidationSource.PARAM),
  postLike.UpdateLike
);

router.delete(
  "/:likeId",
  validator(Schema.likeId, ValidationSource.PARAM),
  postLike.deleleLike
);

export default router;
