import express from "express";
import auth from "./auth/auth";
import users from "./user/user";
import media from "./media/media";
import email from "./email/email";
import userType from "./userType/userType";
import admin from "./admin/admin";
import posts from "./post/post";
import likes from "./postLike/like";
const router = express.Router();

router.use("/auth", auth);
router.use("/users", users);
router.use("/usertypes", userType);
router.use("/admins", admin);
router.use("/posts", posts);
router.use("/:postId/likes", likes);
export default router;
