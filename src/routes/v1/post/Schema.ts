import Joi from "@hapi/joi";
import { PostStatus, PostType } from "../../../database/model/Post";
import { JoiObjectId } from "../../../helpers/validator";

export default {
  postId: Joi.object().keys({
    postId: JoiObjectId().required(),
    mediaFolderName: Joi.string().optional(),
  }),
  createCard: Joi.object().keys({
    scoreCard: Joi.object()
      .keys({
        teamScore: Joi.number().required(),
        opponentScore: Joi.number().required(),
        matchDate: Joi.date().required(),
        goals: Joi.number().required(),
        assists: Joi.number().required(),
        keyPasses: Joi.number().required(),
        nutmegs: Joi.number().required(),
      })
      .required(),
  }),
  createMedia: Joi.object().keys({
    description: Joi.string().optional().min(1).max(500).trim(),
    mediaUrl: Joi.string().optional(),
  }),
  createShort: Joi.object().keys({
    description: Joi.string().optional().min(1).max(200).trim(),
    mediaUrl: Joi.string().optional(),
  }),
  updateCard: Joi.object().keys({
    scoreCard: Joi.object()
      .keys({
        teamScore: Joi.number().integer().optional(),
        opponentScore: Joi.number().integer().optional(),
        matchDate: Joi.date().iso().optional(),
        goals: Joi.number().integer().optional(),
        assists: Joi.number().integer().optional(),
        keyPasses: Joi.number().integer().optional(),
        nutmegs: Joi.number().integer().optional(),
      })
      .optional(),
  }),
  update: Joi.object().keys({
    postType: Joi.string().valid(PostType.MEDIA, PostType.SHORT).optional(),
    description: Joi.string().optional().min(1).max(500).trim(),
  }),
  updateStatus: Joi.object().keys({
    status: Joi.string()
      .valid(PostStatus.ACCEPTED, PostStatus.REJECTED)
      .required(),
  }),
};
