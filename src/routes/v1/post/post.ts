import express from "express";
import uploadMediaFilesToThisFolder from "../../../helpers/fileUpload/uploadDestiny";
import authentication from "../../../auth/authentication";
import authorization from "../../../auth/authorization";
import { RoleCode } from "../../../database/model/Role";
import FileUploadHandler from "../../../helpers/fileUpload/index";
import postController from "../../../controllers/post";

import validator, { ValidationSource } from "../../../helpers/validator";
import Schema from "./Schema";

const router = express.Router();
const fileUploadHandler = new FileUploadHandler();

router.use("/", authentication, authorization([RoleCode.ADMIN, RoleCode.USER]));

// create post
router.post(
  "/card",
  validator(Schema.createCard),
  postController.createPostCard
);

// create post
router.post(
  "/media",
  uploadMediaFilesToThisFolder("posts"),
  fileUploadHandler.handleSingleFileUpload("mediaUrl"),
  validator(Schema.createMedia),
  postController.createPostMedia
);

// create post
router.post(
  "/short",
  uploadMediaFilesToThisFolder("posts"),
  fileUploadHandler.handleSingleFileUpload("mediaUrl"),
  validator(Schema.createShort),
  validator(Schema.createShort),
  postController.createPostShort
);

// get all posts
router.get("/", postController.getPosts);

router.get("/test", postController.getAllPosts);

// get one post
router.get(
  "/:postId",
  validator(Schema.postId, ValidationSource.PARAM),
  postController.getPost
);

// update one  post card
router.patch(
  "/:postId/card",
  validator(Schema.postId, ValidationSource.PARAM),
  validator(Schema.updateCard),
  postController.UpdatePost
);

// update one  post
router.patch(
  "/:postId",
  uploadMediaFilesToThisFolder("posts"),
  fileUploadHandler.handleSingleFileUpload("mediaUrl"),
  validator(Schema.postId, ValidationSource.PARAM),
  validator(Schema.update),
  postController.UpdatePost
);

// update one  post status
router.patch(
  "/:postId/status",
  authorization([RoleCode.ADMIN]),
  validator(Schema.postId, ValidationSource.PARAM),
  validator(Schema.updateStatus),
  postController.UpdatePostStatus
);


router.delete("/:postId",validator(Schema.postId,ValidationSource.PARAM),postController.removePost)

export default router;
