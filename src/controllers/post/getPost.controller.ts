import { ProtectedRequest } from "app-request";
import { Types } from "mongoose";
import {  NotFoundError } from "../../core/ApiError";
import { SuccessResponse } from "../../core/ApiResponse";
import PostRepo from "../../database/repository/PostRepo";
import asyncHandler from "../../helpers/asyncHandler";

export const getPost = asyncHandler(async (req: ProtectedRequest, res) => {
    const postId = new Types.ObjectId(req.params.postId);
   
    const post = await PostRepo.findPostByObj({
      _id: req.params.postId,
    });
 
    if (!post) throw new NotFoundError('Post not found');
    return new SuccessResponse('Post returned successfully', post).send(res);
  });