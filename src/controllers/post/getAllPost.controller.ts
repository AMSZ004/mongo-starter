import { ProtectedRequest } from "app-request";
import { SuccessResponsePaginate } from "../../core/ApiResponse";
import PostRepo from "../../database/repository/PostRepo";
import asyncHandler from "../../helpers/asyncHandler";




export const getPosts = asyncHandler(async(req:ProtectedRequest,res)=>{
    const { page, perPage, deleted } = req.query;
  const options = {
    page: parseInt(page as string, 10) || 1,
    limit: parseInt(perPage as string, 12) || 12,
  };
  const posts = await PostRepo.findAll(options, req.query, {
    isPaging: true,
    deleted: deleted == 'true' ? true : false,
  });
  const { docs, ...meta } = posts;
  new SuccessResponsePaginate('All posts returned successfully', docs, meta).send(res);
})