import { ProtectedRequest } from "app-request";
import { PostType } from "../../database/model/Post";
import { BadRequestError } from "../../core/ApiError";
import { SuccessResponse } from "../../core/ApiResponse";
import PostRepo from "../../database/repository/PostRepo";
import asyncHandler from "../../helpers/asyncHandler";

export const createPostShort = asyncHandler(
  async (req: ProtectedRequest, res) => {
    req.body.postType = PostType.SHORT;
    if (!req.file) throw new BadRequestError("mediaUrl is required");
    req.body.mediaUrl = req.file.filename;
    req.body.mediaType = req.file.mimetype;
    req.body.user = req.user?._id;
    const post = await PostRepo.create(req.body);
    if (!post) new BadRequestError("post error");
    return new SuccessResponse(
      "Post has been created successfully!",
      post
    ).send(res);
  }
);

export const createPostMedia = asyncHandler(
  async (req: ProtectedRequest, res) => {
    req.body.postType = PostType.MEDIA;
    if (!req.file) throw new BadRequestError("mediaUrl is required");

    req.body.mediaUrl = req.file.filename;
    req.body.mediaType = req.file.mimetype;
    req.body.user = req.user?._id;
    const post = await PostRepo.create(req.body);
    if (!post) new BadRequestError("post error");
    return new SuccessResponse(
      "Post has been created successfully!",
      post
    ).send(res);
  }
);

export const createPostCard = asyncHandler(
  async (req: ProtectedRequest, res) => {
    req.body.postType = PostType.CARD;
    req.body.user = req.user?._id;
    const post = await PostRepo.create(req.body);
    if (!post) new BadRequestError("post error");
    return new SuccessResponse(
      "Post has been created successfully!",
      post
    ).send(res);
  }
);
