import  PostRepo  from '../../database/repository/PostRepo';
import { ProtectedRequest } from "app-request";
import { BadRequestError } from "../../core/ApiError";
import { SuccessMsgResponse } from "../../core/ApiResponse";
import asyncHandler from "../../helpers/asyncHandler";





export const removePost = asyncHandler(async (req: ProtectedRequest, res) => {
    const { postId } = req.params;
    const post = await PostRepo.remove(postId);
    if (!post) throw new BadRequestError('Post not found');
    return new SuccessMsgResponse('Post deleted successfully').send(res);
  });