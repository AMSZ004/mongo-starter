import { removePost } from './removePost.contorller';
import {
  createPostCard,
  createPostMedia,
  createPostShort,
} from "./createPost.controller";
import  { getAllPosts }  from "./CustomGetAllPost.controller";
import  { getPosts }  from "./getAllPost.controller";
import { getPost } from "./getPost.controller";
import { UpdatePost, UpdatePostStatus } from "./updatePost.controller";

export default {
  createPostCard,
  createPostMedia,
  createPostShort,
  getPosts,
  getPost,
  UpdatePost,
  UpdatePostStatus,
  removePost,
  getAllPosts
};
