import { ProtectedRequest } from "app-request";
import { NotFoundError } from "../../core/ApiError";
import { SuccessResponse } from "../../core/ApiResponse";
import PostRepo from "../../database/repository/PostRepo";
import asyncHandler from "../../helpers/asyncHandler";
import { Types } from "mongoose";

export const UpdatePost = asyncHandler(async (req: ProtectedRequest, res) => {
  const postId = new Types.ObjectId(req.params.postId);
  const userId = new Types.ObjectId(req.user._id);
  if (req.file) {
    req.body.mediaUrl = req.file.filename;
    req.body.mediaType = req.file.mimetype;
  }
  if (req.body.scoreCard) req.body.scoreCard = JSON.parse(req.body.scoreCard);
  const post = await PostRepo.updateOnePost(
    { _id: postId, user: userId },
    req.body
  );
  if (!post) new NotFoundError("Post not found ");
  new SuccessResponse("Post updated successfully", post).send(res);
});

export const UpdatePostStatus = asyncHandler(
  async (req: ProtectedRequest, res) => {
    const postId = new Types.ObjectId(req.params.postId);
    const post = await PostRepo.updateOnePost({ _id: postId }, req.body);
    if (!post) new NotFoundError("Post not found ");
    new SuccessResponse("Post updated successfully", post).send(res);
  }
);
