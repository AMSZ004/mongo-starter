import { BadRequestError } from "./../../core/ApiError";
import { ProtectedRequest } from "app-request";
import { Types } from "mongoose";
import { NotFoundError } from "../../core/ApiError";
import PostRepo from "../../database/repository/PostRepo";
import asyncHandler from "../../helpers/asyncHandler";
import PostLikeRepo from "../../database/repository/PostLikeRepo";
import { SuccessResponse } from "../../core/ApiResponse";

export const createLike = asyncHandler(async (req: ProtectedRequest, res) => {
  const postId = new Types.ObjectId(req.params.postId);
  const userId = req.user._id;
  const post = await PostRepo.findPostByObj({ _id: postId });

  if (!post) throw new NotFoundError("Post not found");
  const isLike: any = await PostLikeRepo.findOne({
    user: userId,
    post: postId,
  });
  let like;

  if (isLike) {
    like = await PostLikeRepo.deleteOne({ _id: isLike._id, user: userId });
    return new SuccessResponse(
      "Like has been deleted successfully!",
      like
    ).send(res);
  } else {
    like = await PostLikeRepo.create({ user: userId, post: postId });
    return new SuccessResponse(
      "Like has been created successfully!",
      like
    ).send(res);
  }
});
