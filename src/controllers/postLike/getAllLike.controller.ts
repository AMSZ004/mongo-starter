import { NotFoundError } from "./../../core/ApiError";
import { ProtectedRequest } from "app-request";
import { Types } from "mongoose";
import PostRepo from "../../database/repository/PostRepo";
import PostLikeRepo from "../../database/repository/PostLikeRepo";
import asyncHandler from "../../helpers/asyncHandler";
import { SuccessResponsePaginate } from "../../core/ApiResponse";

export const getAllLike = asyncHandler(async (req: ProtectedRequest, res) => {
  const postId = new Types.ObjectId(req.params.postId);
  const { page, perPage, deleted } = req.query;
  const options = {
    page: parseInt(page as string, 10) || 1,
    limit: parseInt(perPage as string, 12) || 12,
  };
  const post = await PostRepo.findPostByObj({ _id: postId });

  if (!post) throw new NotFoundError("Post not found");

  const likes = await PostLikeRepo.findAll(
    options,
    req.query,
    {
      isPaging: true,
      deleted: deleted == "true" ? true : false,
    },
    postId
  );
  const { docs, ...meta } = likes;
  new SuccessResponsePaginate(
    "All likes returned successfully",
    docs,
    meta
  ).send(res);
});
