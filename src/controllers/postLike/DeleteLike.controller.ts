import { SuccessResponse } from "./../../core/ApiResponse";
import { Types } from "mongoose";
import { ProtectedRequest } from "app-request";
import asyncHandler from "../../helpers/asyncHandler";
import PostLikeRepo from "../../database/repository/PostLikeRepo";
import { NotFoundError } from "../../core/ApiError";
import PostRepo from "../../database/repository/PostRepo";

export const deleleLike = asyncHandler(async (req: ProtectedRequest, res) => {
  const postId = new Types.ObjectId(req.params.postId);
  const likeId = req.params.likeId;
  const post = await PostRepo.findPostByObj({ _id: postId });
  if (!post) throw new NotFoundError("Post not found");
  const like = await PostLikeRepo.deleteOne({ _id: likeId });
  if (!like) throw new NotFoundError("Like not found ");
  return new SuccessResponse("like deleted successfully", like).send(res);
});
