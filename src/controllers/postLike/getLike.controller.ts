import { SuccessResponse } from './../../core/ApiResponse';
import { NotFoundError } from './../../core/ApiError';
import { Types } from "mongoose";
import { ProtectedRequest } from "app-request";
import asyncHandler from "../../helpers/asyncHandler";
import PostRepo from "../../database/repository/PostRepo";
import PostLikeRepo from '../../database/repository/PostLikeRepo';

export const getLike = asyncHandler(async (req: ProtectedRequest, res) => {
  const postId = new Types.ObjectId(req.params.postId);
  const likeId = req.params.likeId;
  const post = await PostRepo.findPostByObj({ _id: postId });
  if(!post) throw new NotFoundError("Post not found")
  const like = await PostLikeRepo.findById(likeId)

  return new SuccessResponse('Like returned successfully',like).send(res)
});
