import PostLikeRepo from "../../database/repository/PostLikeRepo";
import { ProtectedRequest } from "app-request";
import { Types } from "mongoose";
import { NotFoundError } from "../../core/ApiError";
import { SuccessResponse } from "../../core/ApiResponse";
import PostRepo from "../../database/repository/PostRepo";
import asyncHandler from "../../helpers/asyncHandler";

export const UpdateLike = asyncHandler(async (req: ProtectedRequest, res) => {
  const postId = new Types.ObjectId(req.params.postId);
  const likeId = new Types.ObjectId(req.params.likeId);
  const post = await PostRepo.findPostByObj({ _id: postId });
  if (!post) new NotFoundError("Post not found ");
  const like = await PostLikeRepo.updateOne(
    {
      _id: likeId,
    },
    req.body
  );
  new SuccessResponse("Like updated successfully", like).send(res);
});
