import { createLike } from "./createLike.controller";
import { deleleLike } from "./DeleteLike.controller";
import { getAllLike } from "./getAllLike.controller";
import { getLike } from "./getLike.controller";
import { UpdateLike } from "./updateLike.controller";



export default {
  createLike,
  getAllLike,
  UpdateLike,
  getLike,
  deleleLike
}