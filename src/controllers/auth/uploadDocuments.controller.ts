import { Types } from 'mongoose';
import asyncHandler from '../../helpers/asyncHandler';
import { ProtectedRequest } from 'app-request';
import { SuccessMsgResponse } from '../../core/ApiResponse';
import { BadRequestError } from '../../core/ApiError';
import VerifDocumentRepo from '../../database/repository/VerifDocumentRepo';
import UserRepo from '../../database/repository/UserRepo';

export const uploadVerifDocuments = asyncHandler(
  async (req: ProtectedRequest, res) => {
    const { userId } = req.params;
    const user = await UserRepo.getOneById(userId);
    if (!user) throw new BadRequestError('invalid ID');
    const files = req.files;
    if (!files) throw new BadRequestError('files not selected');

    const filesArray = files as Express.Multer.File[];

    const documents = filesArray.map((file) => {
      return file.path;
    });

    await VerifDocumentRepo.create({ user: userId, documents });

    return new SuccessMsgResponse('the files have been uploaded').send(res);
  }
);
