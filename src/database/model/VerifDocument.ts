import { model, Schema, Document } from 'mongoose';
import User from './User';
import { mongoosePagination, Pagination } from 'mongoose-paginate-ts';
import { preFindHook } from '../../helpers/databaseHooks';

export const DOCUMENT_NAME = 'VerifDocument';
export const COLLECTION_NAME = 'verifDocuments';

export default interface VerifDocument extends Document {
  user: User;
  documents: string[];
  deletedAt?: Date;
}

const schema = new Schema<VerifDocument>(
  {
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    documents: [String],
    deletedAt: {
      type: Date,
      default: null,
      select: false,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);
preFindHook(schema);
schema.plugin(mongoosePagination);

export const VerifDocumentModel = model<
  VerifDocument,
  Pagination<VerifDocument>
>(DOCUMENT_NAME, schema, COLLECTION_NAME);
