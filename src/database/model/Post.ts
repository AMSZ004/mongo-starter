import { model, ObjectId, Schema } from "mongoose";
import { mongoosePagination, Pagination } from "mongoose-paginate-ts";

export const DOCUMENT_NAME = "Post";
export const COLLECTION_NAME = "posts";
export enum PostType {
  CARD = "CARD",
  MEDIA = "MEDIA",
  SHORT = "SHORT",
}
export enum PostStatus {
  ACCEPTED = "ACCEPTED",
  REJECTED = "REJECTED",
}
interface ScoreCard {
  teamScore: number;
  opponentScore: number;
  matchDate: Date;
  goals: number;
  assists: number;
  keyPasses: number;
  nutmegs: number;
}

export default interface IPost extends Document {
  user?: ObjectId;
  postType: PostType;
  description?: string;
  scoreCard?: ScoreCard;
  mediaUrl?: string;
  mediaType?: string;
  status?: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}

const schema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
    postType: {
      type: Schema.Types.String,
      trim: true,
      enum: [PostType.CARD, PostType.MEDIA, PostType.SHORT],
    },
    description: {
      type: Schema.Types.String,
    },
    scoreCard: {
      teamScore: {
        type: Schema.Types.Number,
      },
      opponentScore: {
        type: Schema.Types.Number,
      },
      matchDate: {
        type: Schema.Types.Date,
      },
      goals: {
        type: Schema.Types.Number,
      },
      assists: {
        type: Schema.Types.Number,
      },
      keyPasses: {
        type: Schema.Types.Number,
      },
      nutmegs: {
        type: Schema.Types.Number,
      },
    },
    mediaUrl: {
      type: Schema.Types.String,
    },
    mediaType: {
      type: Schema.Types.String,
    },
    status: {
      type: Schema.Types.String,
      default: PostStatus.ACCEPTED,
    },
    deletedAt: {
      type: Date,
      select: true,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

schema.plugin(mongoosePagination);
schema.pre(/^find/, function (next) {
  this.populate({
    path: "user",
    select:"_id firstName lastName userName email"
  });
  next();
});
export const PostModel = model<IPost, Pagination<IPost>>(
  DOCUMENT_NAME,
  schema,
  COLLECTION_NAME
);
