import { model, ObjectId, Schema } from "mongoose";
import { mongoosePagination, Pagination } from "mongoose-paginate-ts";
import IPost from "./Post";
import { preFindHook } from '../../helpers/databaseHooks';


export const DOCUMENT_NAME = "Like";
export const COLLECTION_NAME = "likes";

export default interface ILike extends Document {
  user?: ObjectId;
  post?: IPost;
  createdAt?: Date;
  updatedAt?: Date;
}

const schema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
    post: {
      type: Schema.Types.ObjectId,
      ref: "Post",
    },
    deletedAt: {
      type: Date,
      select: true,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);
preFindHook(schema);
schema.plugin(mongoosePagination);
schema.pre(/^find/, function (next) {
  this.populate({
    path: "user",
    select:"_id firstName lastName userName email"
  });
  next();
});
export const LikeModel = model<ILike, Pagination<ILike>>(
  DOCUMENT_NAME,
  schema,
  COLLECTION_NAME
);
