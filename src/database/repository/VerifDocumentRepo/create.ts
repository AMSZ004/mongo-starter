import VerifDocument, { VerifDocumentModel } from '../../model/VerifDocument';

const create = async (obj: object): Promise<VerifDocument> => {
  return await VerifDocumentModel.create(obj);
};

export default create;
