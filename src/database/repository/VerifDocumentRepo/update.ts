import VerifDocument, { VerifDocumentModel } from '../../model/VerifDocument';

const update = async (
  id: string,
  obj: Partial<VerifDocument>
): Promise<VerifDocument | null> => {
  return await VerifDocumentModel.findByIdAndUpdate(
    id,
    { $set: { ...obj } },
    { new: true }
  ).exec();
};

export default update;
