import VerifDocument, { VerifDocumentModel } from '../../model/VerifDocument';

const remove = async (id: string): Promise<VerifDocument | null> => {
  return await VerifDocumentModel.findByIdAndUpdate(
    id,
    { $set: { deletedAt: Date.now() } },
    { new: true }
  ).exec();
};

export default remove;
