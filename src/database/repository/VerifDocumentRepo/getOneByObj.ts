import VerifDocument, { VerifDocumentModel } from '../../model/VerifDocument';

const findByObj = (obj: object): Promise<VerifDocument | null> => {
  return VerifDocumentModel.findOne(obj).exec();
};

export default findByObj;
