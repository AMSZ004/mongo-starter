import VerifDocument, { VerifDocumentModel } from '../../model/VerifDocument';

const findById = (id: string): Promise<VerifDocument | null> => {
  return VerifDocumentModel.findById(id).exec();
};

export default findById;
