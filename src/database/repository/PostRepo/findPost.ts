import IPost, { PostModel } from "../../../database/model/Post";

 const findPostByObj = async (post: object): Promise<IPost | null> => {
  return await PostModel.findOne(post);
};


export default findPostByObj