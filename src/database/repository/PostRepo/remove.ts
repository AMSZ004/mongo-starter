
import IPost, { PostModel } from "../../../database/model/Post";

const remove = async (id: string): Promise<IPost | null> => {
  return await PostModel.findByIdAndUpdate(
    id,
    { $set: { deletedAt: Date.now() } },
    { new: true }
  ).exec();
};

export default remove;
