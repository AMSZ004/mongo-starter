
import { PaginationModel } from 'mongoose-paginate-ts';
import APIFeatures from '../../../helpers/apiFeatures';
import { ApiOptions } from 'app-request';
import IPost, { PostModel } from '../../../database/model/Post';

type pagingObj = {
  limit: number;
  page: number;
};

const findAll = async (
  paging: pagingObj,
  query: object,
  apiOptions: ApiOptions,
): Promise<PaginationModel<IPost>> => {
  let findAllQuery = apiOptions.deleted
    ? PostModel.find({ deletedAt: { $ne: null } })
    : PostModel.find({ deletedAt: null });

  const features = new APIFeatures(findAllQuery, query)
    .filter()
    .sort()
    .limitFields()
    .search([]);

  const options = {
    query: features.query,
    limit: paging.limit ? paging.limit : null,
    page: paging.page ? paging.page : null,
  };

  return (await PostModel.paginate(options)) as PaginationModel<IPost>;
};

export default findAll;
