import IPost, { PostModel } from "../../../database/model/Post";

const create = async (post: IPost): Promise<IPost> => {
  return await PostModel.create(post);
};
export default create;
