
import findPostByObj from "./findPost";
import create from "./create";
import findAll from "./findAllPosts";
import updateOnePost from "./updateOnePost";
import findById from "./findById";
import remove from "./remove";
import findAllCustom from "./findAllCustom";

export default {
  create,
  findAll,
  findPostByObj ,
  updateOnePost,
  findById,
  remove,
  findAllCustom
};
