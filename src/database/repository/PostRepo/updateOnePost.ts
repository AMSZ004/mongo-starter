import IPost from "../../../database/model/Post";
import { PostModel } from "../../../database/model/Post";

const updateOnePost = async (
  postId: object,
  post: object
): Promise<IPost | null> => {
  return await PostModel.findOneAndUpdate(
    postId,
    { $set: post },
    { new: true, runValidators: true }
  );
};
export default updateOnePost;
