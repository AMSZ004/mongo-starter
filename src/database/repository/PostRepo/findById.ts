import IPost from "../../../database/model/Post";
import { PostModel } from "../../..//database/model/Post";

const findById = async (id: string): Promise<IPost | null> => {
  return await PostModel.findById(id);
};
export default findById;
