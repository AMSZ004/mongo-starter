import { ApiOptions } from "app-request";
import { PaginationModel } from "mongoose-paginate-ts";
import IPost, { PostModel } from "../../../database/model/Post";
// Import necessary types

type pagingObj = {
  limit: number;
  page: number;
};

const findAllCustom = async (
  paging: pagingObj,
  query: object,
  apiOptions: ApiOptions,
  reqUser: any // Assuming you have a user object in the request
): Promise<PaginationModel<IPost>> => {
  // Conditionally build the query based on the apiOptions.deleted flag
  let findAllQuery = apiOptions.deleted
    ? { deletedAt: { $ne: null } }
    : { deletedAt: null };

  // Include additional query parameters, if any, based on your 'query' object

  const pipeline = [
    {
      $match: findAllQuery, // Conditionally match based on deleted flag
    },
    {
      $lookup: {
        from: "likes", // Name of the likes collection
        localField: "_id", // Field to match in the posts collection (post _id)
        foreignField: "post", // Field to match in the likes collection (like post)
        as: "likes", // New field in each post document to store likes
      },
    },
    {
      $addFields: {
        userLiked: {
          $in: [reqUser._id, "$likes.user"], // Check if reqUser.id exists in the 'likes.user' array
        },
        likesCount: { $size: "$likes" },
      },
    },
    {
      $project: {
        likes: 0, // Exclude the 'likes' field from the output
      },
    },
  ];

  const options = {
    page: paging.page ? paging.page : 1,
    limit: paging.limit ? paging.limit : 12,
  };

  // You might need to adjust this part based on your schema and data model
  const result: any = await PostModel.aggregate(pipeline).collation({
    locale: "en",
    strength: 2,
  });
  return result;
  // Assuming you have the necessary response model or class for pagination
  /*   return new PaginationModel(result, options.page, options.limit, result.length); */
};

export default findAllCustom;
