import { LikeModel } from "./../../model/PostLike";
import ILike from "../../../database/model/PostLike";

const findById = async (id: string): Promise<ILike | null> => {
  return await LikeModel.findById(id);
};
export default findById;
