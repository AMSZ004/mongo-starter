import { LikeModel } from "./../../model/PostLike";
import ILike from "../../../database/model/PostLike";

const findOne = async (like: object): Promise<ILike | null> => {
  return await LikeModel.findOne(like);
};
export default findOne;
