import ILike, { LikeModel } from "./../../model/PostLike";

const updateOne = async (
  postId: object,
  post: object
): Promise<ILike | null> => {
  return await LikeModel.findOneAndUpdate(
    postId,
    { $set: post },
    { new: true, runValidators: true }
  );
};
export default updateOne;
