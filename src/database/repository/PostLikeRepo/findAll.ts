import { LikeModel } from "./../../model/PostLike";

import { PaginationModel } from "mongoose-paginate-ts";
import APIFeatures from "../../../helpers/apiFeatures";
import { ApiOptions } from "app-request";
import ILike from "../../../database/model/PostLike";

type pagingObj = {
  limit: number;
  page: number;
};

const findAll = async (
  paging: pagingObj,
  query: object,
  apiOptions: ApiOptions,
  post: Object
): Promise<PaginationModel<ILike>> => {
 
  let findAllQuery = apiOptions.deleted
    ? LikeModel.find({ post: post, deletedAt: { $ne: null } })
    : LikeModel.find({ post: post, deletedAt: null });

  const features = new APIFeatures(findAllQuery, query)
    .filter()
    .sort()
    .limitFields()
    .search([]);

  const options = {
    query: features.query,
    limit: paging.limit ? paging.limit : null,
    page: paging.page ? paging.page : null,
  };

  return (await LikeModel.paginate(options)) as PaginationModel<ILike>;
};

export default findAll;
