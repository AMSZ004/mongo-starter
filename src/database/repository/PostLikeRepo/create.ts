import ILike, { LikeModel } from "../../../database/model/PostLike";

const create = async (like: object): Promise<ILike> => {
  return await LikeModel.create(like);
};
export default create;
