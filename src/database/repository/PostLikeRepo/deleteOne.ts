import { LikeModel } from './../../model/PostLike';
import ILike from "../../../database/model/PostLike";



const deleteOne = async (obj:object): Promise<ILike | null>=>{
    return await LikeModel.findByIdAndDelete(obj)
}
export default deleteOne;